;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> Syntax objects encapsulating Scheme datums together with
;;> source-location information.

(define-library (rapid syntax)
  (export make-source-location
	  source-location?
	  source-location-source
	  source-location-start-line
	  source-location-end-line
	  source-location-start-column
	  source-location-end-column
	  source-location->string
	  source-location->prefix
          make-syntax
          syntax?
	  syntax-set-datum!
	  unwrap-syntax
	  syntax-location
	  syntax->datum
	  datum->syntax
	  syntax
	  syntax-match ... -> unquote guard
	  current-syntax-context
	  syntax-comparator
	  syntax-error-object?
	  syntax-error-object-type
	  syntax-error-object-syntax
	  syntax-error-object-message
	  syntax-error-object->string
	  raise-syntax-error raise-syntax-warning raise-syntax-note raise-syntax-fatal-error
	  syntax->vicinity)  
  (import (except (scheme base) quasiquote)
	  (scheme case-lambda)
	  (scheme write)
	  (rapid quasiquote)
	  (rapid match)
	  (rapid format)
	  (rapid list)
	  (rapid identity)
	  (rapid comparator)
	  (rapid assume)
	  (rapid receive)
	  (rapid mapping)
	  (rapid vicinity))
  (include "syntax.scm"))

;; Local Variables:
;; eval: (put 'syntax-match 'scheme-indent-function 1)
;; eval: (font-lock-add-keywords 'scheme-mode
;;                               '(("(\\(syntax-match\\)\\>" 1 font-lock-keyword-face)))
;; End:
