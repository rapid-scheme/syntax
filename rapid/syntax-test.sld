;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid syntax-test)
  (export run-tests)
  (import (scheme base)
	  (rapid test)
	  (rapid comparator)
	  (rapid syntax)
	  (rapid vicinity))
  (begin
    (define (run-tests)
      (test-begin "Syntax")

      (test-group "Source locations"
        (define location (make-source-location "file" 1 2 3 4))
	(define location/same-line (make-source-location "file" 1 2 1 4))
	
        (test-assert "source-location?"
	  (source-location? location))

	(test-equal "source-location-source"
	  "file"
	  (source-location-source location))

	(test-equal "source-location-start-line"
	  1
	  (source-location-start-line location))

	(test-equal "source-location-start-column"
	  2
	  (source-location-start-column location))

	(test-equal "source-location-end-line"
	  3
	  (source-location-end-line location))
	
	(test-equal "source-location-end-column"
	  4
	  (source-location-end-column location))

	(test-equal "source-location->string"
	  "file:1.3-3.4"
	  (source-location->string location))

	(test-equal "source-location->string: start and end on the same line"
	  "file:1.3-4"
	  (source-location->string location/same-line))
	
	(test-equal "source-location->prefix"
	  "file:1.3-3.4: "
	  (source-location->prefix location)))

      (test-group "Syntax"
        (define location (make-source-location "/share/file" 1 2 3 4))
	(define foo (make-syntax 'foo location))
	  
	(test-assert "syntax?"
	  (syntax? foo))

	(test-eq "unwrap-syntax"
	  'foo
	  (unwrap-syntax foo))
	
	(test-eq "syntax-location"
	  location
	  (syntax-location foo))

	(test-assert "datum->syntax"
	  (syntax? (datum->syntax foo '(if bar baz qux))))

	(syntax-set-datum! foo 'bar)	
	(test-eq "syntax-set-datum!"
	  'bar
	  (unwrap-syntax foo))

	(test-assert "syntax"
	  (syntax? (syntax (if x y ,(* 10 2)))))

	(test-equal "current-syntax-context"
	  location
	  (parameterize ((current-syntax-context foo))
	    (syntax-location (syntax bar))))
	
	(test-assert "syntax-comparator"
	  (comparator? syntax-comparator))

	(test-equal "syntax->vicinity"
	  "/share/baz.scm"
	  (in-vicinity (syntax->vicinity foo) "baz.scm"))
	
	(test-group "Vectors"
	  (define bar (make-syntax 'bar location))
	  (define baz (make-syntax 'baz location))
	  (define vec (make-syntax (vector bar baz) location))

	  (test-equal "unwrap-syntax"
	    (vector bar baz)
	    (unwrap-syntax vec))

	  (test-equal "syntax->datum"
	    #(bar baz)
	    (syntax->datum vec)))
	
	(test-group "Dotted lists"
	  (define bar (make-syntax 'bar location))
	  (define baz (make-syntax 'baz location))
	  (define dotted (make-syntax (cons bar baz) location))

	  (test-equal "unwrap-syntax"
	    (cons bar baz)
	    (unwrap-syntax dotted))

	  (test-equal "syntax->datum"
	    '(bar . baz)
	    (syntax->datum dotted)))

	(test-group "Circular list"
	  (define bar (make-syntax 'bar location))
	  (define baz (make-syntax 'baz location))
	  (define datum
	    (let ((datum (list bar baz)))
	      (set-cdr! (cdr datum) (cdr datum))
	      datum))
	  (define circ
	    (make-syntax datum location))

	  (test-eq "unwrap-syntax"
	    datum
	    (unwrap-syntax circ))

	  (test-equal "syntax->datum"
	    '(bar . #0=(baz . #0#))
	    (syntax->datum circ))))

      (test-group "Syntax matcher"

	(test-group "Automatic syntax unwrapping"
	  (define location (make-source-location "file" 1 2 3 4))
	  (define context (make-syntax 'context location))
	  
	  (test-equal "Syntax match"
	    'bar
	    (syntax-match (syntax context bar)
	      (bar 'bar)
	      (,_ #f)))
	  
	  (test-equal "Variables carry syntax"
	    (list location location)
	    (syntax-match (syntax context (bar (x y z)))
	      ((bar (,x . ,r)) (list (syntax-location x) (syntax-location (car r))))
	      (,_ #f))))

	(test-group "Syntax wrapping in the right context"
	  (define location1 (make-source-location "file1" 1 2 3 4))
	  (define location2 (make-source-location "file2" 0 0 0 0))
	  (define context1 (make-syntax 'context1 location1))
	  (define context2 (make-syntax 'context2 location2))
	  (test-equal ""
	    (list 'baz "file2")
	    (syntax-match (syntax context1 (bar ,(syntax context2 foo)))
	      ((bar ,(x)) (list (syntax->datum x) (source-location-source (syntax-location x))))
	      (,foo (syntax baz))))))

      (test-group "Syntax error objects"
        (define location (make-source-location "file" 1 2 3 4))
	(define foo (make-syntax 'foo location))
		
	(test-equal "raise-syntax-error"
	  "file:1.3-3.4: error: this is an error"
	  (guard
	      (condition ((syntax-error-object? condition)
			  (syntax-error-object->string condition)))
	    (raise-syntax-error foo "this is an ~a" "error"))))
      
      (test-end))))
