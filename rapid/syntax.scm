;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Syntax objects

;;> \section{Syntax objects}

;;> A \emph{syntax object} consists of an unwrapped syntax object
;;> together with source location information about the unwrapped
;;> syntax object.  An \emph{unwrapped syntax object} is either a
;;> vector whose elements are syntax objects, a pair whose car is a
;;> syntax object and whose cdr is either a syntax object or the empty
;;> list, or any other Scheme value.

;;; Syntax

;;> \section{Syntax}

;;> \macro{(syntax <context> <datum>)\br{}
;;> (syntax <datum>)}

;;> Converts the \scheme{<datum>} recursively into a syntax object by
;;> wrapping each piece that is not already a syntax object with the
;;> source location information of the syntax object that is the
;;> result of evaluating \scheme{<context>}.  If \scheme{<context>} is
;;> missing, the current syntactic context (see below) is used.  It is
;;> an error if the expansion of \var{<datum>} contains any cycles.

;;> \scheme{Quasiquote} (\scheme{`}), \scheme{unquote} (\scheme{,}),
;;> \scheme{unquote-splicing} (\scheme{,@}), and \scheme{...} inside
;;> \scheme{<datum>} have the same meaning as if \code{<datum>}
;;> appeared at the top nesting level inside a quasiquotation.  The
;;> meaning of \code{...} is as in the quasiquotation syntax exported
;;> by \code{(rapid quasiquote}).

(define-syntax syntax
  (syntax-rules ()
    ((syntax context datum)
     (datum->syntax context `datum))
    ((syntax datum)
     (datum->syntax (current-syntax-context) `datum))))

;;> \macro{(syntax-match <expr> <clause> ...)}

;;> Equivalent to \scheme{(match <expr> <clause> ...)} from
;;> \code{(rapid match)} with the following exceptions: Firstly, the
;;> current syntactic context in the \code{<clause>}s is changed to
;;> that of the result of evaluating \code{<expr>} should it evaluate
;;> to a syntax object.  Secondly, while matching the result of
;;> evaluating \code{<expr>} against the clauses, syntax objects are
;;> automatically unwrapped if needed.

(define-syntax syntax-match
  (syntax-rules ()
    ((syntax-match expr . clauses)
     (let loop ((%expr expr))
       (parameterize ((current-syntax-context %expr))
	 (match* (unwrap-syntax loop) %expr . clauses))))))

;;; Procedures

;;> \section{Procedures}

(define-record-type <source-location>
  (%make-source-location source start-line start-column end-line end-column)
  source-location?
  (source source-location-source)
  (start-line source-location-start-line)
  (start-column source-location-start-column)
  (end-line source-location-end-line)
  (end-column source-location-end-column))

;;> \procedure{(make-source-location source start-line start-column
;;> end-line end-column)}

;;> Returns a source location with the fields (see below) as specified.

(define (make-source-location source start-line start-column end-line end-column)
  (assume (exact-integer? start-line))
  (assume (exact-integer? start-column))
  (assume (exact-integer? end-line))
  (assume (exact-integer? end-column))
  (%make-source-location source start-line start-column end-line end-column))

;;> \procedure{(source-location? obj)}
 
;;> Returns \scheme{#t} if \scheme{obj} is a source location, and
;;> \scheme{#f} otherwise.

;;> \procedure{(source-location-source source-location)}
;;> Returns the source (an arbitrary value, often a path) of the the
;;> \scheme{source-location}, identifying its source.

;;> \procedure{(source-location-start-line source-location)}
;;> Returns the line number (counting from 1) of the starting position
;;> of the \scheme{source-location}.

;;> \procedure{(source-location-start-column source-location)} Returns
;;> the column number (counting from 0) of the starting position of
;;> the \scheme{source-location}.

;;> \procedure{(source-location-end-line source-location)}
;;> Returns the line number (counting from 1) of the ending position
;;> of the \scheme{source-location}.

;;> \procedure{(source-location-end-column source-location)}
;;> Returns the column number (counting from 0) of the ending position
;;> of the \scheme{source-location}.

;;> \procedure{(source-location->string source-location)}

;;> Converts the \scheme{source-location} into a string for use in
;;> error messages.  The format follows the
;;> \hyperlink[https://www.gnu.org/prep/standards/html_node/Errors.html]{GNU
;;> Coding Standards}.

(define (source-location->string source-location)
  (if source-location
      (let ((port (open-output-string)))
	(write-string (source-location-source source-location) port)
	(write-string ":" port)
	(let
	    ((start-line (source-location-start-line source-location))
	     (end-line (source-location-end-line source-location))
	     (start-column (+ (source-location-start-column source-location) 1))
	     (end-column (source-location-end-column source-location)))
	  (display start-line port)
	  (write-string "." port)
	  (display start-column port)
	  (write-string "-" port)
	  (unless (= start-line end-line)
	    (display end-line port)
	    (write-string "." port))
	  (display end-column port))
	(get-output-string port))
      ""))

;;> \procedure{(source-location->prefix source-location)}

;;> Equivalent to \scheme{(source-location->string source-location)}
;;> except that the string \scheme{": "} is added to the result if it
;;> is not the empty string.

(define (source-location->prefix source-location)
  (let ((string (source-location->string source-location)))
    (if (zero? (string-length string))
	string
	(string-append string ": "))))

;; Syntax objects

(define-record-type <syntax>
  (%make-syntax datum location identity)
  syntax?
  (datum syntax-datum syntax-set-datum!)
  (location syntax-location)
  (identity syntax-identity))

;;> \procedure{(make-syntax datum source-location)}

;;> Returns a new syntax object consisting of the unwrapped syntax
;;> object \scheme{datum} and \scheme{source-location}.

;;> \procedure{(syntax? obj)}

;;> Returns \scheme{#t} if \scheme{obj} is a source location, and
;;> \scheme{#f} otherwise.

(define (make-syntax datum location)
  (assume (or (not location) (source-location? location)))
  (%make-syntax datum location (generate-identity)))

;;> \procedure{(unwrap-syntax syntax)}

;;> Returns the unwrapped syntax object inside the syntax object \scheme{syntax}.  Returns
;;> \scheme{syntax} itself if it is not a syntax object,

(define (unwrap-syntax syntax)
  (if (syntax? syntax)
      (syntax-datum syntax)
      syntax))

;;> \procedure{(syntax->datum syntax strip)\br{}
;;> (syntax->datum syntax)}

;;> Recursively apply \scheme{unwrap-syntax} to the syntax object
;;> \scheme{syntax}.  Moreover, unwrapped syntax objects that are
;;> neither pairs, vectors, or lists are mapped by the procedure
;;> \scheme{strip} taking one argument and returning one value.  If
;;> \scheme{strip} is not given, it defaults to a procedure returning
;;> its argument.  \scheme{syntax->datum} is able to handle cycles in
;;> \scheme{syntax}.

(define syntax->datum
  (case-lambda
    ((syntax) (syntax->datum syntax (lambda (datum) datum)))
    ((syntax strip)
     (define datums (mapping syntax-comparator))
     (let syntax->datum ((syntax syntax))
       (define (failure)
	 (let ((datum (unwrap-syntax syntax)))
	   (cond
	    ((vector? datum)
	     (let* ((n (vector-length datum))
		    (vec (make-vector n)))
	       (set! datums (mapping-set datums syntax vec))
	       (do ((i 0 (+ i 1)))
		   ((>= i n))
		 (vector-set! vec i (syntax->datum (vector-ref datum i))))
	       vec))
	    ((pair? datum)
	     (receive (n tail)
		 (length+tail datum)
	       (let ((lst (make-list n)))
		 (set! datums (mapping-set datums syntax lst))
		 (let loop ((datum datum) (n n) (lst lst) (converted-tail #f))
		   (let ((converted-tail
			  (if (eq? tail datum)
			      lst
			      converted-tail)))
		     (cond
		      ((= n 1)
		       (set-car! lst (syntax->datum (car datum)))	   
		       (cond
			((pair? tail)
			 (set-cdr! lst converted-tail))
			((not (null? tail))
			 (set-cdr! lst (syntax->datum tail)))))
		      (else
		       (let ((converted-datum (syntax->datum (car datum))))
			 (set-car! lst (syntax->datum (car datum)))
			 (loop (cdr datum)
			       (- n 1)
			       (cdr lst)
			       converted-tail))))))
		 lst)))
	    (else
	     (strip datum)))))
       (mapping-ref datums syntax failure)))))

;;> \procedure{(datum->syntax context datum)}

;;> Converts \scheme{datum} recursively into a syntax object by
;;> wrapping each piece that is not already a syntax object with the
;;> source location information of the syntax object \scheme{<ontext}.
;;> It is an error if \scheme{datum} contains cycles.

(define (datum->syntax context datum)
  (let*
      ((location (if context (syntax-location context) #f))
       (make-syntax
	(lambda (datum)
	  (make-syntax datum location))))
    (let datum->syntax ((datum datum))
      (cond
       ((syntax? datum)
	datum)
       ((vector? datum)
	(make-syntax (vector-map datum->syntax datum)))
       ((pair? datum)
	(receive (elements)
	    (let loop ((datum datum))
	      (cond
	       ((null? datum)
		'())
	       ((pair? datum)
		(cons (datum->syntax (car datum))
		      (loop (cdr datum))))
	       (else
		(datum->syntax datum))))
	  (make-syntax elements)))
       (else
	(make-syntax datum))))))

;;; Utility procedures

(define (length+tail obj)
  (let loop ((hare obj) (tortoise obj) (m 0))
    (if (pair? hare)
	(let ((hare (cdr hare)))
	  (if (pair? hare)
	      (let ((hare (cdr hare))
		    (tortoise (cdr tortoise)))
		(if (eq? hare tortoise)
		    (values (+ m 2) hare)
		    (loop hare tortoise (+ m 2))))
	      (values (+ m 1) hare)))
	(values m hare))))

;; Syntax errors

;;> \procedure{(syntax-error-object? obj)}

;;> Returns \scheme{#t} if \var{obj} is a syntax error object, and
;;> \scheme{#f} otherwise.

;;> \procedure{(syntax-error-object-type syntax-error-object)}

;;> Returns the type of \var{syntax-error-object}.  Possible types are
;;> the symbols \scheme{error}, \scheme{warning}, \scheme{note}, and
;;> \scheme{fatal-error}.

;;> \procedure{(syntax-error-object-syntax syntax-error-object)}

;;> Returns the syntax object associated with
;;> \var{syntax-error-object}.

;;> \procedure{(syntax-error-object-message syntax-error-object)}

;;> Returns the error message associated with
;;> \var{syntax-error-object}.

(define-record-type <syntax-error-object>
  (make-syntax-error-object type syntax message)
  syntax-error-object?
  (type syntax-error-object-type)
  (syntax syntax-error-object-syntax)
  (message syntax-error-object-message))

(define (raise-syntax-error-object type syntax format-string objects)
  (let ((raise (if (eq? 'fatal-error type) raise raise-continuable)))
    (raise (make-syntax-error-object type
				     syntax
				     (apply format format-string
					    objects)))
    #f))

;;> \procedure{(raise-syntax-error syntax format-string object ...)}

;;> Raises a syntax error object of type \scheme{error} associated
;;> with \var{syntax} where the error message is given by the
;;> \code{(rapid format)} \scheme{format-string} whose escape
;;> sequences are replaced by the \var{objects}.  The exception is
;;> continuable.  When the procedure returns, it returns \scheme{#f}.

(define (raise-syntax-error syntax format-string . objects)
  (raise-syntax-error-object 'error syntax format-string objects))

;;> \procedure{(raise-syntax-warning syntax format-string object ...)}

;;> Raises a syntax error object of type \scheme{warning} associated
;;> with \var{syntax} where the error message is given by the
;;> \code{(rapid format)} \scheme{format-string} whose escape
;;> sequences are replaced by the \var{objects}.  The exception is
;;> continuable.  When the procedure returns, it returns \scheme{#f}.

(define (raise-syntax-warning syntax format-string . objects)
  (raise-syntax-error-object 'warning syntax format-string objects))

;;> \procedure{(raise-syntax-note syntax format-string object ...)}

;;> Raises a syntax error object of type \scheme{note} associated
;;> with \var{syntax} where the error message is given by the
;;> \code{(rapid format)} \scheme{format-string} whose escape
;;> sequences are replaced by the \var{objects}.  The exception is
;;> continuable.  When the procedure returns, it returns \scheme{#f}.

(define (raise-syntax-note syntax format-string . objects)
  (raise-syntax-error-object 'note syntax format-string objects))

;;> \procedure{(raise-syntax-fatal-error syntax format-string object ...)}

;;> Raises a syntax error object of type \scheme{fatal-error}
;;> associated with \var{syntax} where the error message is given by
;;> the \code{(rapid format)} \scheme{format-string} whose escape
;;> sequences are replaced by the \var{objects}.

(define (raise-syntax-fatal-error syntax format-string . objects)
  (raise-syntax-error-object 'fatal-error syntax format-string objects))

;;> \procedure{(syntax-error-object->string syntax-error-object)}

;;> Converts the \var{syntax-error-object} into a string for use in
;;> error messages.  The format follows the
;;> \hyperlink[https://www.gnu.org/prep/standards/html_node/Errors.html]{GNU
;;> Coding Standards}.

(define (syntax-error-object->string syntax-error)
  (let ((syntax (syntax-error-object-syntax syntax-error)))
    (if syntax
	(format "~a~a: ~a"
		(source-location->prefix (syntax-location syntax))
		(syntax-error-object-name syntax-error)
		(syntax-error-object-message syntax-error))
	(format "~a: ~a"
		(syntax-error-object-name syntax-error)
		(syntax-error-object-message syntax-error)))))

(define (syntax-error-object-name syntax-error)
  (case (syntax-error-object-type syntax-error)
    ((error) "error")
    ((warning) "warning")
    ((note) "note")
    ((fatal-error) "fatal-error")
    (else (assume #f))))

;;> \procedure{(syntax-vicinity syntax-object)}

;;> Returns the vicinity of the source location of the
;;> \var{syntax-object}.

(define (syntax->vicinity syntax)
  (let ((location (syntax-location syntax)))    
    (pathname->vicinity (if location
			    (source-location-source location)
			    ""))))

;;; Parameter objects

;;> \section{Parameter objects}

(define secret (vector #f))

;;> \procedure{current-syntax-context}

;;> Parameter object holding the current syntactic context, a syntax
;;> object.

(define current-syntax-context
  (make-parameter secret
		  (lambda (syntax)
		    (cond
		     ((syntax? syntax)
		      syntax)
		     ((eq? secret syntax)
		      #f)
		     (else
		      (current-syntax-context))))))

;;; Comparators

;;> \section{Comparators}

(define (syntax-ordering-predicate s1 s2)
  (< (syntax-identity s1) (syntax-identity s2)))

(define (syntax-hash-function s)
  (syntax-identity s))

;;> \procedure{syntax-comparator}

;;> Comparator used to compare syntax objects.  It provides both
;;> ordering and hash functions.

(define syntax-comparator
  (make-comparator syntax? eq? syntax-ordering-predicate syntax-hash-function))

(comparator-register-default! syntax-comparator)
